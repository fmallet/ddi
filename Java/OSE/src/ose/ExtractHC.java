package ose;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.gargoylesoftware.htmlunit.WebResponse;
import com.gargoylesoftware.htmlunit.html.DomNodeList;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlForm;
import com.gargoylesoftware.htmlunit.html.HtmlInput;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlSelect;

public class ExtractHC {
	static final String hcUrl = "voir-heures-comp/code:" ; //+ ID ;//+ "?annee=2019&type-volume-horaire=2&etat-volume-horaire=1&submit=appliquer";
	static private String[] typesPermanent = { "Composante", "Service", "Modification", "Du", 
			"Service-Ref", "Assure", 
			"HC-Ref", "HC", "Solde" };
	static private int []   posPermanent   = { 1, 2, 3, 4, 8, 9, -3, -2, -1 };
	static private String[] typesVacataire = { 
			"Service-Ref", "Assure", 
			"HC-Ref", "HC", "Solde" };
	static private int []   posVacataire   = { 4, 5, -3, -2, -1 };
	// 5 : Service FI
	// 6 : Service FA
	// 7 : Service FC
	// 8 : Service référentiel
	// 10 : ? (empty)
	// 11 : HC FI
	// 12 : HC FA
	// 13 : HC FC
	// 14 : FC article
	// 15 : HC référentiel
	// 16 : HC
	// 17 : Solde
	
	
	private HtmlPage page;
	
	public ExtractHC(HtmlPage pageHC, int year, int type, int etat) throws MalformedURLException, IOException {
		page = getContentHC(pageHC, year, type,	etat); 
	}

	boolean read(HashMap<String, Object> values) {
		if (page == null) return false;
		
		//System.out.println(p2.asText());
		HtmlElement contenu = (HtmlElement)page.getElementById("content");
//		System.out.println(contenu.asXml());
		List<HtmlElement> el = contenu.getElementsByAttribute("div", "class", "row");

		String[] types = typesPermanent;
		int[] pos = posPermanent;
		
		if (el.size() == 14) { // vacataire
			types = typesVacataire;
			pos = posVacataire;
		}
		for (int i = 0; i < types.length; i++) {
			Object v = DownloadFromOse.stringToObject(getValue(el, pos[i]));
			if (v != null) values.put(types[i], v);
		}
		return true;
	}	

	private static String getValue(List<HtmlElement> list, int index) {
//		System.out.println(index + " / " + list.size());
		HtmlElement el = list.get((index<0?list.size():0)+index);
		return el.getFirstChild().getNextElementSibling().getNextElementSibling().asNormalizedText();
	}
	private HtmlPage getContentHC(HtmlPage pageHC, int year, int type, int etat)
			throws IOException, MalformedURLException {

		{
			WebResponse response = pageHC.getWebResponse();
			int code = response.getStatusCode();
			if (code != 200) {
				Logger.getLogger("fr.univ-cotedazur.ose").log(Level.WARNING, "Failing HC:" + code);
				return null;
			}
		}
		HtmlSelect select = (HtmlSelect) pageHC.getElementById("annee");
		select.getOptionByValue("2020").removeAttribute("selected");
		HtmlPage p1= select.setSelectedAttribute(year+"", true);

		List<HtmlForm> forms = p1.getForms();
		DomNodeList<HtmlElement>  selects = forms.get(0).getElementsByTagName("select");
		for (HtmlElement el : selects) {
			HtmlSelect sel = (HtmlSelect)el;
			if (sel.getAttribute("name").equals("type-volume-horaire")) {
				sel.getOptionByValue(type+"").setSelected(true); 					
			} else if (sel.getAttribute("name").equals("etat-volume-horaire")) {
				sel.getOptionByValue(etat+"").setSelected(true);				
			}
		}
		HtmlInput in = forms.get(0).getInputByName("submit");
		HtmlPage p2 = in.click();
		return p2;
	}
	

	static String getUrl(int harpege) {
		return hcUrl + harpege;
	}
}
