package ose;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;

public class EtatCivil {
	private HashMap<String, Object> etatCivil = new HashMap<>();
	private DateTimeFormatter df = DateTimeFormatter.ofPattern("dd/MM/yyyy");
	public EtatCivil(int harpege, LocalDate birthdate) {
		etatCivil.put("Harpege", harpege);
		etatCivil.put("Birthdate", df.format(birthdate));
		addAge(birthdate);
	}

//	final static private String[] entry = {"Name", "Gender", "Birthdate", "Email", "Statut", "Section", "Grade"};
	final static private String[] entry = {"Gender", "Name", /*"Type",*/ "Statut", "Composante"}; // less fields in 2021
	void display(PrintWriter pw, DownloadFromOse dfo) {
		dfo.display(pw, etatCivil);
	}
	
	public void register(Object[] res) {
		for (int i = 0; i < entry.length; i++) {
			etatCivil.put(entry[i], res[i]);
		}
	}	
	
	private void addAge(LocalDate birth) {
		if (birth == null) return; // no birth date => no age
		
//			LocalDate birth = LocalDate.of(Integer.parseInt(s[2]), Integer.parseInt(s[1]), Integer.parseInt(s[0]));
		Period p = Period.between(birth, LocalDate.now());
			
		double age = p.getYears() + p.getMonths() / 12.0 + p.getDays() / 365;
		etatCivil.put("Age", age);
//			System.out.println("\t=>" + df.format(age));
	}
	
	static PrintWriter makeFile(String path, boolean fileOrString) throws IOException {
		Writer w;
		if (fileOrString) 
			w = new FileWriter(new File(path, "civil.yml"));
		else
			w = new StringWriter(); 
		PrintWriter pw = new PrintWriter(w);
		pw.println("metadata:");
		pw.println("  date: " + new Date());		
		pw.println("");
		pw.println("harpege:");		
		return pw;
	}
}
