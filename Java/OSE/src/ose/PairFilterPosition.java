package ose;

public class PairFilterPosition {
	private IFilter<?> filter;
	private int pos;
	public PairFilterPosition(int pos) {
		this(null, pos);
	}
	public PairFilterPosition(IFilter<?> filter, int pos) {
		this.filter = filter;
		this.pos = pos;
	}
	public Object extract(String[] values) {
		if (filter == null) return values[pos];
		return filter.convert(values[pos]);
	}
}
