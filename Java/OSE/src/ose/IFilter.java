package ose;

public interface IFilter<T> {
	T convert(String s);
}
