package ose;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Read n<sup>th</sup> input as an integer 
 * @author fmallet
 *
 */
public class ReadCSV {
	private File file;

	ReadCSV(String path, String filename) {
		file = new File(path, filename + ".csv");
		if (! file.exists()) throw new RuntimeException("Input file " + file + " does not exist !");	
	}
	List<Object[]> readAll(PairFilterPosition...pos) throws FileNotFoundException {
		try (Scanner sc = new Scanner(file)) {
			LinkedList<Object[]> numbers = new LinkedList<>();
			w1 : while (sc.hasNext()) {
				String[] line = sc.nextLine().split(";");
				Object[] res = new Object[pos.length];
				for (int i = 0; i < pos.length; i++) {
					res[i] = pos[i].extract(line);
					if (res[i] == null) {
						System.err.println("Null entry: " + Arrays.toString(line));
						continue w1;
					}
				}
				numbers.add(res);
			}
			return numbers;
		}		
	}
	List<Integer> readAll(int pos) throws FileNotFoundException {
		try (Scanner sc = new Scanner(file)) {
			LinkedList<Integer> numbers = new LinkedList<>();
			while (sc.hasNext()) {
				String[] line = sc.nextLine().split(";");
				try {
					int harpege = Integer.parseInt(line[pos]);
					numbers.add(harpege);
				} catch(NumberFormatException nfe) {	
					Logger.getLogger("fr.univ-cotedazur.ose").log(Level.WARNING, "Not an integer " + Arrays.toString(line));
				}
			}
			return numbers;
		}
	}

}
