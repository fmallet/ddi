package ose;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

import ose.filter.DateFilter;
import ose.filter.IntegerFilter;

public class DownloadFromOse {
	static final String LOGGER_OSE = "fr.univ-cotedazur.ose";
	
	static final int ID = 78600;
	static final int YEAR = 2020;
	static final int TYPE = 2; // réalisé
	static final int ETAT = 1; // validé

	static final String baseUrl = "https://ose.univ-cotedazur.fr/intervenant/";

	private EtatCivil etatCivil;
	private Service service;
	
	static private boolean first = true;

	private WebClient client;
	private int harpege;

	public DownloadFromOse(WebClient client, int harpege) {
		this.client = client;
		this.harpege = harpege;
	}

	// now the birthdate comes from the csv file (before it was coming from OSE)
	boolean readFiche(LocalDate birthdate) throws FailingHttpStatusCodeException, MalformedURLException, IOException {
		etatCivil = new EtatCivil(harpege, birthdate);
		HtmlPage pageFiche = client.getPage(baseUrl + ExtractFiche.getUrl(harpege));

		Object[] res = new ExtractFiche(pageFiche,
				new String[] {"Civilité :", "NOM prénom :"}, // removed BirthDate in 2021
//				new String[] {"Civilité :", "NOM prénom :", "Date de naissance :"},
				new String[] {},  // email is not here any more
//				new String[] {"Email :"},
//				new String[] {"Type d'intervenant :", "Discipline :", "Grade :"}).extract(first); // no more access to Grade
				new String[] {"Statut de l'intervenant :", "Affectation principale :"}).extract(first); // removed Type, Discipline, Grade in 2021

		first = false;

		if (res == null) return false;

		etatCivil.register(res);
		return true;
	}
	void readHeuresComp(int year, int type, int etat) throws MalformedURLException, IOException {
		service = new Service (harpege);

		client.getOptions().setJavaScriptEnabled(true);		
		HtmlPage pageHC = client.getPage(baseUrl + ExtractHC.getUrl(harpege));
//		client.getOptions().setJavaScriptEnabled(false);

		service.extractHC(pageHC, year, type, etat);		
	}		

	private static NumberFormat df = NumberFormat.getNumberInstance(Locale.US); 
	static {
		df.setMaximumFractionDigits(2);
	}
	
	private void display(PrintWriter pwEc, PrintWriter pwService) {
		etatCivil.display(pwEc, this);
		if (READ_SERVICE)
			service.display(pwService, this);
	}
	void display(PrintWriter pw, Map<String, Object> values) {
		String init = "  - ";
		for (String s : values.keySet()) {
			Object o = values.get(s);
			if (o instanceof Double) {
				o = df.format(o);
			}
			pw.println(init + s + ": " + values.get(s));
			init = "    ";
		}
	}

	static boolean READ_SERVICE = false; // false if wants to update only the EtatCivil and not the Service
	static boolean WRITE_CIVIL = true; // false when do not want to overwrite existing civil file
	public static void main(String[] args) throws FailingHttpStatusCodeException, MalformedURLException, IOException {
		String PATH = "/home/fmallet/git/_WWW/ddi.github.io/data";

		ReadCSV reader = new ReadCSV(PATH, "2021-2022/FicheRH27");
		List<Object[]> numbers = reader.readAll(new PairFilterPosition(IntegerFilter.INSTANCE, 1),
				new PairFilterPosition(DateFilter.INSTANCE, 5));
		int number = numbers.size();
		System.out.println(number + " entries from FicheRH27.");

		PrintWriter err = new PrintWriter(new File(PATH, "err.txt"));

		java.util.logging.Logger.getLogger("com.gargoylesoftware.htmlunit").setLevel(Level.OFF); 

		WebClient client = new WebClient(BrowserVersion.BEST_SUPPORTED);
		// for some reasons, javascript was required in 2020
		// in 2021, it generates an exception 
		client.getOptions().setJavaScriptEnabled(false); //Important, enabling JS

		try (PrintWriter pwEc = EtatCivil.makeFile(PATH, WRITE_CIVIL); 
			 PrintWriter pwS = Service.makeFile(PATH, YEAR, TYPE, ETAT)) {
			int i = 0;
			for (Object[] person : numbers) {
				int harpege = (Integer)person[0];
				if (harpege < 0) { number--; continue; }
				LocalDate birthdate = (LocalDate)person[1];
				System.out.println("Harpege: " + harpege + " (" + (i+1) + "/" + number + ")");
				try {
					DownloadFromOse dfo = new DownloadFromOse(client, harpege);
					if (dfo.readFiche(birthdate) && READ_SERVICE) {
						dfo.readHeuresComp(YEAR, TYPE, ETAT);
					}
					dfo.display(pwEc, pwS);
					err.println(harpege + " : OK");
				} catch (FailingHttpStatusCodeException e) {
					err.println("Failing for " + harpege);
				}
				i++;
			}
			client.close();
		}
		err.close();
	}

	static Object stringToObject(String s) {
		s = s.trim();
		if (s.length() == 0) return null;
		try {
			double d = Double.parseDouble(s.replace(',', '.'));
			return d;
		} catch (NumberFormatException nfe) {
			return s;
		}
	}	
}
