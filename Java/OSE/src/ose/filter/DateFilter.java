package ose.filter;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.logging.Level;
import java.util.logging.Logger;

import ose.IFilter;

public class DateFilter implements IFilter<LocalDate> {
	public static final DateFilter INSTANCE = new DateFilter();
	private static final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
//	private static final SimpleDateFormat df = new SimpleDateFormat("dd/mm/yy");
	private DateFilter() {
		// SINGLETON
	}
	@Override
	public LocalDate convert(String s) {
		try {
			return LocalDate.parse(s, dtf);
		} catch (DateTimeParseException pe) {
			Logger.getLogger("fr.univ-cotedazur.ose").log(Level.WARNING, "WrongDate: " + s);
			return null;
		}
	}

}
