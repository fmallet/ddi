package ose.filter;

import java.util.logging.Level;
import java.util.logging.Logger;

import ose.IFilter;

public class IntegerFilter implements IFilter<Integer> {
	public static final IntegerFilter INSTANCE = new IntegerFilter();
	private IntegerFilter() {
		// SINGLETON
	}
	@Override
	public Integer convert(String s) {
		try {
			return Integer.parseInt(s);			
		} catch (NumberFormatException nfe) {
			Logger.getLogger("fr.univ-cotedazur.ose").log(Level.WARNING, "Not an integer: " + s);
			return -1;
		}
	}

}
