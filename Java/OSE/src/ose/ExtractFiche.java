package ose;

import java.io.IOException;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

import com.gargoylesoftware.htmlunit.WebResponse;
import com.gargoylesoftware.htmlunit.html.DomNode;
import com.gargoylesoftware.htmlunit.html.HtmlDefinitionList;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

public class ExtractFiche {
	static final String ficheUrl = "/voir";
	
	private HtmlPage page;
	String[] etatCivilFields, infoFields, statusFields;

	static String getUrl(int harpege) {
//		return harpege + ficheUrl;		
		return "code:" + harpege + ficheUrl; // 2021: add code: otherwise id instead of harpege		
	}
	
	ExtractFiche(HtmlPage page, String[] etatCivilFields, String[] infoFields, String[] statusFields) {
		this.page = page;
		this.etatCivilFields = etatCivilFields;
		this.infoFields = infoFields;
		this.statusFields = statusFields;
	}
	
	Object[] extract(boolean first) throws IOException {
		HtmlElement fiche = getFiche(first);
		if (fiche != null)
			return parse(fiche);
		return null;
	}
	
	private HtmlElement getFiche(boolean first) throws IOException {
		{
			WebResponse response = page.getWebResponse();
			int code = response.getStatusCode();
			if (code != 200) {
				Logger.getLogger("fr.univ-cotedazur.ose").severe(code + " : " + response);
				return null;
			}
		}

		HtmlPage content = page;
		if (first) {
			Connection c = new Connection(page);
			content = c.connect("fmallet");
		}

		if (content == null) return null;

		HtmlElement fiche = (HtmlElement)content.getElementById("fiche");
		if (fiche == null) {
			JOptionPane.showMessageDialog(null, "Invalid password !", "Invalid password", JOptionPane.ERROR_MESSAGE);
			return null;
		}		
		return fiche;
	}
	
	private Object[] parse(HtmlElement fiche) {
		HtmlDefinitionList etatCivil = (HtmlDefinitionList)fiche.getFirstChild();
		Object[] res = new Object[etatCivilFields.length + infoFields.length + statusFields.length];
		
		int pos = find(etatCivil, res, 0, etatCivilFields);
		
		HtmlDefinitionList info = etatCivil;// info is not there anymore, skip
//		HtmlDefinitionList info = (HtmlDefinitionList)etatCivil.getNextSibling();
//		pos += find(info, res, pos, infoFields);
		
		HtmlDefinitionList statut = (HtmlDefinitionList)info.getNextSibling();	
		find(statut, res, pos, statusFields);
		
		return res;
	}

	// assumes alternation of dt and dd 
	// gives the dt, find the dd that goes along
	private int find(HtmlDefinitionList list, Object[] res, int shift, String ...dt) {
//		System.out.println(list.asText());
		int curPos = 0;
		DomNode current = list.getFirstChild();
		while (current!= null && curPos < dt.length) {
			if (current.getTextContent().equals(dt[curPos])) {
				DomNode dd = current.getNextSibling();
				res[curPos + shift] = DownloadFromOse.stringToObject(dd.getTextContent()); 
				curPos++;
			}
			current = current.getNextSibling();
		}
		return curPos;
	}	
}
