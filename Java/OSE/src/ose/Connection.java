package ose;

import java.io.IOException;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import com.gargoylesoftware.htmlunit.html.HtmlForm;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlPasswordInput;
import com.gargoylesoftware.htmlunit.html.HtmlSubmitInput;
import com.gargoylesoftware.htmlunit.html.HtmlTextInput;

public class Connection {
	private HtmlPage page;
	
	Connection(HtmlPage page) {
		this.page = page;
	}
	
	HtmlPage connect(String username) throws IOException {
		JPanel panel = new JPanel();
		JLabel userLabel = new JLabel("Username:");
		JTextField userTF = new JTextField(username);
		JLabel labelPass = new JLabel("Password:");
		JPasswordField pass = new JPasswordField(10);
		panel.add(userLabel);
		panel.add(userTF);
		panel.add(labelPass);
		panel.add(pass);
		pass.requestFocusInWindow();
		String[] options = new String[]{"OK", "Cancel"};
		int option = JOptionPane.showOptionDialog(null, panel, "The title",
		                         JOptionPane.NO_OPTION, JOptionPane.PLAIN_MESSAGE,
		                         null, options, options[0]);
		if(option == JOptionPane.OK_OPTION)
		{
		    char[] password = pass.getPassword();
		    username = userTF.getText();
		    return connect(username, new String(password));
		}
		return null;
	}
	
	HtmlPage connect(String username, String password) throws IOException {
		HtmlForm form = (HtmlForm)page.getElementById("fm1");
		
		HtmlTextInput usernameInput = form.getInputByName("username");
		usernameInput.setValueAttribute(username);
		
		HtmlPasswordInput passwordInput = form.getInputByName("password");
		passwordInput.setValueAttribute(password);
		
		HtmlSubmitInput submit = form.getInputByName("submit");
		return submit.click();		
	}
}
