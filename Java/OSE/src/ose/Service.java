package ose;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.util.Date;
import java.util.HashMap;

import com.gargoylesoftware.htmlunit.html.HtmlPage;

public class Service {
	private HashMap<String, Object> service = new HashMap<>();

	public Service(int harpege) {
		service.put("Harpege", harpege);
	}

	public void extractHC(HtmlPage pageHC, int year, int type, int etat) throws MalformedURLException, IOException {
		new ExtractHC(pageHC, year, type, etat).read(service);		
	}

	public void display(PrintWriter pw, DownloadFromOse downloadFromOse) {
		downloadFromOse.display(pw, service);
	}

	static PrintWriter makeFile(String path, int year, int type, int etat) throws FileNotFoundException {
		PrintWriter pw;
		if (DownloadFromOse.READ_SERVICE) 
			pw = new PrintWriter(new File(path, "service-" + year + "-" + type + "-" + etat + ".yml"));
		else 
			pw = new PrintWriter(new StringWriter()); // writes into a string but should not be used.
		pw.println("metadata:");
		pw.println("  date: " + new Date());
		pw.println("  year: " + year);
		pw.println("  type-volume-horaire: " + type);
		pw.println("  etat: " + etat);
		pw.println();
		pw.println("services:");
		return pw;
	}
}
