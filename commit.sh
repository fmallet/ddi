#!/bin/bash
cp _config.yml docs/
date=$(date '+%Y-%m-%d %H:%M:%S')
git commit -a -m "${1:-update members ${date}}"
git push
