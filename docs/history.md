---
layout: default
title: DDI
---

# Département Disciplinaire Informatique

## Objectif

Les départements disciplinaires ont été créées dans les statuts de l'établissement expériemental en juillet 2019 pour avoir une vue consolidée de toutes les intervenants intérieurs et extérieurs dans les activités pédagogiques d'Université Côte d'Azur.

Il est consulté par la gouvernance, les composantes ou les laboratoires pour chaque question qui touche au potentiel humain pour l'Informatique.



##  Historique

- 12 mars 2020 : Un directeur et deux directeurs adjoints sont nommés par le président pour construire les statuts et préparer une élection
  - **Directeur provisoire :** Frédéric Mallet
  - **Directeurs Adjoints :** Michel Riveill, Michel Syska
- mars 2021 : les règles de fonctionnement sont publiées et présentées aux membres lors de plusieurs séances publiques organisées en visio
  - [Presentation]({% link assets/pdf/DDI.pdf %})
- 4 avril 2021 : Une élection est organisée pour élire un directeur par l'ensemble des membres du département disciplinaire
  - **Directeur :** Frédéric Mallet 
- 25 mai 2022 : Une élection est organisée pour remplacer le directeur démissionnaire
  - **Directeur :** Enrico Formenti
