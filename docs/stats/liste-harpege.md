---
layout : stats-c
---
# Membres
**Date:** {{ site.data.civil.metadata.date }}

## <a name="alpha"></a>Alphabétique 

[-> Ancienneté](#anc)

{% assign k = "Harpege, Name, Section, composante, departement, Age" | split: ", " %}
{% assign etatcivil = site.data.civil.harpege | selectSomeKeys: k | arraytodict: 'Harpege' %} 
{% assign service = site.data.ddi.membres | selectSomeKeys: k | arraytodict: 'Harpege' %} 
{% assign data = service | mergeHashes: etatcivil | dicttoarray %}

{% assign alpha = data | sort:'Name' %}
{% assign anc = data | sort:'Age'  | reverse %}

Name | Composante | Departement | Section
--- |  --- | --- | ---
{%for e in alpha %}{{e.Name}} | {{e.composante}} | {{e.departement | dept}} | {{e.Section| slice: 0,2}}
{%endfor %}

## <a name="anc"></a>Ancienneté

[-> Alphabétique](#alpha)

Name | Composante | Departement | Section
--- | --- | --- | --- 
{%for e in anc %}{{e.Name}} | {{e.composante}} | {{e.departement | dept}} | {{e.Section | slice: 0,2 }} 
{%endfor %}
