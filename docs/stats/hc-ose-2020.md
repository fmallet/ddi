---
layout : stats-c
---
# Charge OSE

{% assign service = site.data._2020.service %}

##  {{service.metadata.year}}-{{service.metadata.year | plus: 1}} 

- **Date:** {{ service.metadata.date }} **Type:** {{ service.metadata.type-volume-horaire }}  **Etat:** {{ service.metadata.etat }}

{% assign k = "Service, Modification, Assure, Solde" | split: ", " %}
{% assign hT = service.services | sum: k | roundh: 2 %}
{% assign etpT = hT | divh: 192 | roundh: 2 %}
{% assign hr = service.services | sort: "Harpege" %}

--- | Service (S) | Modif (M) | Dû = S + M | HC | Dû + HC
--- | --- | --- | --- | --- | --- 
{% assign duT = hT.Service | plus: hT.Modification %}{% assign hcT = hT.Assure | plus: hT.Solde | minus: duT %}**h** | {{hT.Service}} | {{hT.Modification}} | {{ duT }} | {{ hcT }} | {{ duT | plus: hcT }}
{% assign etpDuT = etpT.Service | plus: etpT.Modification %}**ETP** | {{ etpT.Service }} | {{ etpT.Modification }} | {{ etpDuT }} | {{ etpT.Assure | plus: etpT.Solde | minus: etpDuT }} | {{ duT | plus: hcT | divided_by: 192 | round: 2}}

<table style="border: 0pt; padding: 0pt;">
<tr><td style="text-align: left;"><strong>Dû</strong> représente les heures de service statutaires moins les modifications pour mise en disponibilité ou décharge pour responsabilités. Heures effectives = Dû + HC </td><td rowspan=2>{{ service.services | piechart }}</td></tr>
<tr><td style="text-align: left;"><font style="color: red">Ne prend pas en compte les interventions en informatique des autres départements disciplinaires (permanents ou vacataires) !</font></td></tr>
</table>

## Composante

{% assign cpte = "SCIENCES, IUT, POLYTECH" | split: ", "%}
Composante | Service | Modif | Dû | HC
--- | --- | --- | --- | --- 
{% for c in cpte %}{% assign s = service.services | where: 'Composante', c %}{% assign h = s | sum: k | roundh: 2%}{% assign etp = h | divh: 192 | roundh: 2 %}{% assign p = h | percent: hT %}{% assign du = h.Service | plus: h.Modification %}{% assign hc = h.Assure | plus: h.Solde | minus: du %}**{{c}} (h)** | {{h.Service}} | {{h.Modification}} | {{du}} | {{hc}}
{% assign etpDu = du | divided_by: 192 | round: 2 %}**(ETP)** | {{ etp.Service }} | {{ etp.Modification }} | {{ etpDu }} | {{ etp.Assure | plus: etp.Solde | minus: etpDu}}
{{c}}/Total | {{ p.Service }}% | {{ p.Modification }}% | {{ du | times: 100 | divided_by: duT | round: 2 }}% | {{ hc | times: 100 | divided_by: hcT | round: 2 }}%
{% endfor %}
