---
layout: membres
composante: IUT
dept: 
  - DPT GESTION DES ENTREPRISES ET DES ADMINISTRATIONS
  - DPT INFORMATIQUE
  - DPT QUALITE LOGISTIQUE INDUSTRIELLE ET ORGANISATION
  - DPT RESEAUX ET TELECOMMUNICATION
  - DPT STATISTIQUE ET INFORMATIQUE DECISIONNELLE
  - DPT TECHNIQUE DE COMMERCIALISATION NICE
---

**Date:** {{ site.data.ddi.date.day }}/{{ site.data.ddi.date.month }}/{{ site.data.ddi.date.year }}

{% for d in page.dept %}

## {{ d }}

 {% for item in site.data.ddi.membres %}
  {% if item.composante contains page.composante %} 
  {% if item.departement contains d %}
- {{ item.name }} {{ item.firstname | capitalize }}
  {% endif %}
  {% endif %}
 {% endfor %}
{% endfor %}
