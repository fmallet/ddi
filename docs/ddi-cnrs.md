---
layout: membres
composante: CNRS
dept: 
  - Section 6
  - Section 7
---
**Date:** {{ site.data.ddi.date.day }}/{{ site.data.ddi.date.month }}/{{ site.data.ddi.date.year }}

{% for d in page.dept %}

## {{ d }}

 {% for item in site.data.ddi.membres %}
  {% if item.composante contains page.composante %} 
  {% if item.departement contains d %}
- {{ item.name }} {{ item.firstname | capitalize }}
  {% endif %}
  {% endif %}
 {% endfor %}
{% endfor %}
