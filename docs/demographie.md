---
layout : stats
title : Répartition démographique
---
## Ensemble du département disciplinaire d'Informatique

{% assign dage = site.data.civil.harpege | map: "Age" | sort | mula: 0.2 | floora | mula: 5 | counta %}
{% assign total = dage | dicttoarray | suma %}
{% assign dage19 = site.data._2019.civil.harpege | map: "Age" | sort | mula: 0.2 | floora | mula: 5 | counta %}
{% assign t19 = dage19 | dicttoarray | suma %}
{% assign dage20 = site.data._2020.civil.harpege | map: "Age" | sort | mula: 0.2 | floora | mula: 5 | counta %}
{% assign t20 = dage20 | dicttoarray | suma %}
 
<!--
Total {%for k in  dage %} | [{{k[0]}}-{{k[0] | plus:5}}[ {% endfor %}
--- {%for k in  dage %} | --- {% endfor %}
{{total}} {%for k in  dage %} | {{k[1]}} {% endfor %}
-->

Membres: 2021: {{total}} 2020: {{t20}} 2019: {{t19}}

<svg width="100%" height="85" style="border: 1px solid black">
<text text-anchor="start" x="0" y="80" class="neutral">Age</text>
<text text-anchor="start" x="0" y="15" class="female">Nombre (2021)</text>
{%for k in dage %}{% assign y = k[1] | times: -2 | plus: 70 %}{% assign x = forloop.index0 | times: 10 | plus: 8 %}{% assign last = k[0] %}<!--{{k[0]}}-->
<text text-anchor="middle" x="{{x}}%" y="80" class="neutral">{{k[0]}}</text>
<rect x="{{x | plus: 2.5}}%" y="{{y}}" width="5%" height="{{k[1] | times: 2}}" class="male"/>
<line x1="{{x | plus: 2.5}}%" y1="75" x2="{{x | plus: 7.5}}%" y2="75" stroke="black"/>
<text text-anchor="middle" x="{{x | plus: 5}}%" y="{{y | minus: 5}}" class="female">{{k[1]}}</text>
{% endfor %}
<text text-anchor="middle" x="{{dage | size | times: 10 | plus: 8}}%" y="80" class="neutral">{{ last | plus: 5}}</text>
</svg>

<svg width="100%" height="85" style="border: 1px solid black">
<text text-anchor="start" x="0" y="80" class="neutral">Age</text>
<text text-anchor="start" x="0" y="15" class="female">Nombre (2020)</text>
{%for k in dage20 %}{% assign y = k[1] | times: -2 | plus: 70 %}{% assign x = forloop.index0 | times: 10 | plus: 8 %}{% assign last = k[0] %}<!--{{k[0]}}-->
<text text-anchor="middle" x="{{x}}%" y="80" class="neutral">{{k[0]}}</text>
<rect x="{{x | plus: 2.5}}%" y="{{y}}" width="5%" height="{{k[1] | times: 2}}" class="male"/>
<line x1="{{x | plus: 2.5}}%" y1="75" x2="{{x | plus: 7.5}}%" y2="75" stroke="black"/>
<text text-anchor="middle" x="{{x | plus: 5}}%" y="{{y | minus: 5}}" class="female">{{k[1]}}</text>
{% endfor %}
<text text-anchor="middle" x="{{dage20 | size | times: 10 | plus: 8}}%" y="80" class="neutral">{{ last | plus: 5}}</text>
</svg>

<svg width="100%" height="85" style="border: 1px solid black">
<text text-anchor="start" x="0" y="80" class="neutral">Age</text>
<text text-anchor="start" x="0" y="15" class="female">Nombre (2019)</text>
{%for k in dage19 %}{% assign y = k[1] | times: -2 | plus: 70 %}{% assign x = forloop.index0 | times: 10 | plus: 8 %}{% assign last = k[0] %}<!--{{k[0]}}-->
<text text-anchor="middle" x="{{x}}%" y="80" class="neutral">{{k[0]}}</text>
<rect x="{{x | plus: 2.5}}%" y="{{y}}" width="5%" height="{{k[1] | times: 2}}" class="male"/>
<line x1="{{x | plus: 2.5}}%" y1="75" x2="{{x | plus: 7.5}}%" y2="75" stroke="black"/>
<text text-anchor="middle" x="{{x | plus: 5}}%" y="{{y | minus: 5}}" class="female">{{k[1]}}</text>
{% endfor %}
<text text-anchor="middle" x="{{dage19 | size | times: 10 | plus: 8}}%" y="80" class="neutral">{{ last | plus: 5}}</text>
</svg>

## Par composante

{% assign k = "Harpege, composante, departement, age, corps" | split: ", " %}
{% assign data = site.data.ddi.membres | selectSomeKeys: k %}
{% assign cpte = data | collect: "composante" | sort %}
{% for cp in cpte %}
 {% assign dc = data | where: "composante", cp | map: "age" | clean | sort | mula: 0.2 | floora | mula: 5 | counta %}
{% assign tc = dc | dicttoarray | suma %}
{{cp}}: {{tc}} 
<svg width="100%" height="60" style="border: 1px solid black">
<text text-anchor="start" x="2" y="55" class="neutral">Age</text>
<text text-anchor="start" x="2" y="15" class="female">Nombre</text>
{%for k in dc %}{% assign y = k[1] | times: -3 | plus: 45 %}{% assign x = forloop.index0 | times: 10 | plus: 7 %}{% assign last = k[0] %}<!--{{k[0]}}-->
<text text-anchor="middle" x="{{x}}%" y="55" class="neutral">{{k[0]}}</text>
<rect x="{{x | plus: 2.5}}%" y="{{y}}" width="5%" height="{{k[1] | times: 3}}" class="male"/>
<line x1="{{x | plus: 2.5}}%" y1="50" x2="{{x | plus: 7.5}}%" y2="50" stroke="black"/>
<text text-anchor="middle" x="{{x | plus: 5}}%" y="{{y | minus: 5}}" class="female">{{k[1]}}</text>
{% endfor %}
<text text-anchor="middle" x="{{dc | size | times: 10 | plus: 7}}%" y="55" class="neutral">{{ last | plus: 5}}</text>
</svg>
{% endfor %}

## Par corps

{% assign corps = data | collect: "corps" | sort %}
{% for co in corps %}
 {% assign dc = data | where: "corps", co | map: "age" | clean | sort | mula: 0.2 | floora | mula: 5 | counta %}
{% assign tc = dc | dicttoarray | suma %}
{% if tc != 1 %}
{{co}}: {{tc}} 
<svg width="100%" height="60" style="border: 1px solid black">
<text text-anchor="start" x="2" y="55" class="neutral">Age</text>
<text text-anchor="start" x="2" y="15" class="female">Nombre</text>
{%for k in dc %}{% assign y = k[1] | times: -2 | plus: 45 %}{% assign x = forloop.index0 | times: 10 | plus: 7 %}{% assign last = k[0] %}<!--{{k[0]}}-->
<text text-anchor="middle" x="{{x}}%" y="55" class="neutral">{{k[0]}}</text>
<rect x="{{x | plus: 2.5}}%" y="{{y}}" width="5%" height="{{k[1] | times: 2}}" class="male"/>
<line x1="{{x | plus: 2.5}}%" y1="50" x2="{{x | plus: 7.5}}%" y2="50" stroke="black"/>
<text text-anchor="middle" x="{{x | plus: 5}}%" y="{{y | minus: 5}}" class="female">{{k[1]}}</text>
{% endfor %}
<text text-anchor="middle" x="{{dc | size | times: 10 | plus: 7}}%" y="55" class="neutral">{{ last | plus: 5}}</text>
</svg>
{% endif %}
{% endfor %}

<!--
{{ site.data.ddi.membres | age }}
-->
