---
layout : default
title : Instances
---
# Instances 

## Comité de pilotage 

Les **membres votants** (cf. Article 9 des règles de fonctionnement) :

- Le directeur ou la directrice du DDI 
- Pour chaque composante (DS4H, IUT, Polytech), 2 représentant.e.s rattaché.e.s principalement à la composante et choisi.e.s parmi les membres du DDI 
- Un représentant.e des chercheur.e.s Inria, membre du DDI 
- Un représentant.e des chercheur.e.s CNRS, membre du DDI 

Les membres **invités permanents** (sans droit de vote) :

- les responsables des diplômes mention informatique ou Miage
- un membre invité pour chaque département pédagogique qui a des membres dans le DDI mais aucun membre votant

## Comité des postes 27

Le comité des postes se réunit à la demande de l'EUR DS4H pour interclasser les demandes de postes en 27 avant arbitrage par le copil de DS4H
Il est composé :

- des 9 membres votants du comité de pilotage du DDI
- des 3 directeurs ou directrices de composantes (DS4H, IUT, Polytech) ou de leur représentant.e.s
- du directeur ou de la directrice du laboratoire I3S ou de son représentant
- des 4 représentants des équipes du comité de direction I3S

## Réunions
*Mode expérimental entre mars 2020 et mars 2021*

Les comptes rendus de réunions sont transférés sur le [wiki du département disciplinaire](https://wiki.univ-cotedazur.fr/display/DDI/).
