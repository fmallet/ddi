---
layout: membres
composante: Département Disciplinaire Informatique
---
**Date:** {{ site.data.ddi.date.day }}/{{ site.data.ddi.date.month }}/{{ site.data.ddi.date.year }}

##  POLYTECH

|  SCIENCES INFORMATIQUES (SI) |  |  | 25 |
|---|---|---|---|
|ABCHICHE Nadia|BAUDE Francoise|CAMINADA Alexandre|COLLAVIZZA Helene|
|COLLET Philippe|COUSTE Mathias|FARON Catherine|GAETANO Marc|
|~~HERMENIER Fabien~~|LAVIROTTE Stephane|LINGRAND Diane|LIPPI Sylvain|
|LITOVSKY Igor|LOPEZ Dino|MARTINET Jean|MOLINES Guilhem|
|OCCELLO Audrey|PINNA Anne marie|POURTIER Remi|PRECIOSO Frederic|
|RENARD Helene|RIVEILL Michel|ROUDIER Yves|TIGLI Jean yves|
|WINCKLER Marco|||

|  GENIE BIOLOGIQUE (GB) |  |  | 2 |
|---|---|---|---|
|BERNOT Gilles|COMET Jean-paul||

|  INGENIERIE SYSTEMES ELECTRONIQUES (ISE) |  |  | 1 |
|---|---|---|---|
|GRANET Vincent|||

**Total:** 27 **H :** 20 (74.07%) **F :**  8 (29.63%) **On leave:** 1

##  IUT

|  INFORMATIQUE |  |  | 14 |
|---|---|---|---|
|ACUNDEGER Erol|BLAY Mireille|BRENAC Lise|CABRIO Elena|
|CAILLOUET Christelle|FERRY Nicolas|MENIN Aline|MOULIERAC Joanna|
|PALLEZ Denis|PALLEZ-DARTIGUES Christel|PERALDI-FRATI Marie agnes|POURCELOT Nicolas|
|RENEVIER Philippe|REY Gaetan||

|  QUALITE LOGISTIQUE INDUSTRIELLE ET ORGANISATION |  |  | 3 |
|---|---|---|---|
|BENSMAIL Julien|FEDELE Carine|HUET Fabrice|

|  RESEAUX ET TELECOMMUNICATION |  |  | 5 |
|---|---|---|---|
|BOUDAOUD Karima|ESCAZUT Cathy|GAUTERO Michel|SASSATELLI Lucile|
|URVOY-KELLER Guillaume|||

|  STATISTIQUE ET INFORMATIQUE DECISIONNELLE |  |  | 1 |
|---|---|---|---|
|DA COSTA PEREIRA Celia|||

|  GESTION DES ENTREPRISES ET DES ADMINISTRATIONS |  |  | 1 |
|---|---|---|---|
|GARCIA Christine|||

|  TECHNIQUE DE COMMERCIALISATION NICE |  |  | 1 |
|---|---|---|---|
|HOHWILLER Luc|||

**Total:** 25 **H :** 11 (44.00%) **F :**  14 (56.00%)

##  INRIA

|  TITANE |  |  | 1 |
|---|---|---|---|
|ALLIEZ Pierre|||

|  NEO |  |  | 2 |
|---|---|---|---|
|ALOUF Sara|NEGLIA Giovanni||

|  DIANA |  |  | 2 |
|---|---|---|---|
|BARAKAT Chadi|LEGOUT Arnaud||

|  STAMP |  |  | 1 |
|---|---|---|---|
|BERTOT Yves|||

|  STARS |  |  | 1 |
|---|---|---|---|
|BREMOND Francois|||

|  ABS |  |  | 1 |
|---|---|---|---|
|CAZALS Frederic|||

|  GraphDeco |  |  | 1 |
|---|---|---|---|
|CORDONNIER Guillaume|||

|  COATI |  |  | 2 |
|---|---|---|---|
|COUDERT David|NISSE Nicolas||

|  KAIROS |  |  | 2 |
|---|---|---|---|
|DE SIMONE Robert|LIQUORI Luigi||

|  WIMMICS |  |  | 1 |
|---|---|---|---|
|GANDON Fabien|||

|  BioVision |  |  | 1 |
|---|---|---|---|
|WU Hui-yin|||

**Total:** 15 **H :** 13 (86.67%) **F :**  2 (13.33%)

##  EUR DS4H

|  INFORMATIQUE |  |  | 37 |
|---|---|---|---|
|APARICIO PARDO Ramon|BALDELLON Olivier|BEAUQUIER Bruno|BRIDOUX Florian|
|BUFFA Michel|~~CAROMEL Denis~~|CRESCENZO Pierre|CRESPELLE Christophe|
|~~DALLE Olivier~~|DE MARIA Elisabetta|DEANTONI Julien|DI GIUSTO Cinzia|
|DONATI Leo|FORMENTI Enrico|GUINGNE Franck|JULIA Sandrine|
|KOUNALIS Emmanuel|LAHIRE Philippe|LETURC Christopher|LOZES Etienne|
|~~LOZI Jean-pierre~~|MALAPERT Arnaud|MALLET Frederic|MARTIN Bruno|
|MENEZ Gilles|MIRBEL Isabelle|PASQUIER Nicolas|PELLEAU Marie|
|REGIN Jean-charles|SAUVAGE Nathalie|SYSKA Michel|TETTAMANZI Andrea|
|TOUATI Sid|TOUNSI Stephane|URSO Pascal|WINTER Michel|
|XU Chuan|||

**Total:** 34 **H :** 30 (88.24%) **F :**  7 (20.59%) **On leave:** 3

##  CNRS

|  Section 6 |  |  | 10 |
|---|---|---|---|
|GIROIRE Frederic|HAVET Frederic|HOGIE Luc|MONTAGNAT Johan|
|MUZY Alexandre|NATALE Emanuele|PASQUIER Claude|PERENNES Stephane|
|RICHARD Adrien|VILLATA Serena||

|  Section 7 |  |  | 1 |
|---|---|---|---|
|LEBRUN Jerome|||

**Total:** 11 **H :** 9 (81.82%) **F :**  2 (18.18%)
