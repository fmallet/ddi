# Declare module of your plugin under Jekyll module
module Jekyll::Sum
  def pie(centre,rayon,angle,fill='None',color='black')
    d = [centre[0] + rayon[0] * Math::cos(angle[0]),
         centre[1] - rayon[1] * Math::sin(angle[0])]
    e = [centre[0] + rayon[0] * Math::cos(angle[1]),
         centre[1] - rayon[1] * Math::sin(angle[1])]
    rot = 0
    rot = 1 if angle[1] < angle[0] 
    la = 0
    la= 1 if (angle[1]-angle[0]).abs() > Math::PI
    r=%{
  <path fill="#{fill}" stroke="#{color}"
        d="M #{centre[0]} #{centre[1]}
           L #{d[0]} #{d[1]}
           A #{rayon[0]},#{rayon[1]} 0,#{la},#{rot} #{e[0].round(0)},#{e[1].round(1)} Z" />
    } 
  end
  def get(hash, key)
    if hash[key]
       hash[key]
    else
       0
    end
  end
  def bilan(input)
    t = input.map{|e| [get(e,'Service') + get(e,'Modification'), get(e,'Assure') + get(e,'Solde')]}.reduce{|sum,num| [sum[0]+num[0],sum[1]+num[1]]}
  end
  def piechart(input)
    t = bilan(input)
    tt=t[0]/t[1]
    tprop = tt*2*Math::PI
    tpns = bilan(input.select{|e| get(e, 'Composante')=='POLYTECH'})
    tiut = bilan(input.select{|e| get(e, 'Composante')=='IUT'})
    teur = bilan(input.select{|e| get(e, 'Composante')=='SCIENCES'})
    c = [teur, tiut, tpns]
    p = c.map{|s,hc| [s/t[0], hc/t[0]]}
    sol = [[50,73,40,80], [75,50,45,90]]
    ss = sol[1] if tt < 0.5
    ss = sol[0] if tt >= 0.5
    r=%{
<svg width="100" height="100" viewport="0 0 100 100">
  <circle r="50" cx="50" cy="50" stroke='rgb(0,126,161)' fill='None'/>
    #{pie([50,50], [50,50], [0, tprop], 'rgb(0,126,161)', 'None')} 
    <text text-anchor="middle" font-weight="bold"
          x="#{ss[0]}%" y="#{ss[2]-15}%" class="male">Dû</text>
    <text text-anchor="middle" font-weight="bold"
          x="#{ss[1]}%" y="#{ss[3]-15}%" class="female">HC</text>
    <text text-anchor="middle" x="#{ss[0]}%" y="#{ss[2]}%" class="male">(#{(t[0]*100/t[1]).round(2)}%)</text>
    <text text-anchor="middle" x="#{ss[1]}%" y="#{ss[3]}%" class="female">(#{(100-t[0]*100/t[1]).round(2)}%)</text>
</svg>
    }
  end

  def suma(array)
    array.reduce(:+)
  end
  # input is an array of hash
  # output is a hash with the number of element of input for each value of key
  def counta(input)
    res = {}
    input.each{|v|
      res[v] = 0 unless res[v]
      res[v] += 1
    }
    res
  end
  def count(input, key)
    res = {}
    input.each{|e|
      res[e[key]] = 0 if e[key] and not res[e[key]]
      res[e[key]] += 1 if e[key]
    }
    res
  end 
  # input is an array of hash
  # output is a hash that floor each entry with key
  def sum(input, key)
     res = {}
     key.each{|k| res[k] = 0}
     input.each{|v| key.each{|k| res[k] += v[k] if v[k]}}
     res
  end
  def cat(input,input2,sep='') 
    input.map{|k,v| [k, v.to_s + sep + input2[k].to_s]}.to_h
  end
  def concatstr(input,str) 
    input.map{|k,v| [k, v.to_s + str]}.to_h
  end
  def floora(input) 
    input.map{|v| v.floor()}
  end
  def ceila(input) 
    input.map{|v| v.ceil()}
  end
  def clean(input)
    input.select{|v| v unless v.nil?}
  end
  def rounda(input,r) 
    input.map{|v| v.round(r)}
  end
  def roundh(input,r) 
    input.map{|k,v| [k, v.round(r)]}.to_h
  end
  def proportion(input)
    t = 0
    input.each{|k,v| t+= v} 
    input.map{|k,v| [k, v*100.0/t]}.to_h
  end
  def percent(input, total)
    input.map{|k,v| [k, (v*100/total[k]).round(0)] }.to_h
  end
  def mula(input, d)
    input.map{|v| v*d}
  end
  def divh(input, d)
    input.map{|k,v| [k, v/d]}.to_h
  end
  # input is an array of dict
  # return dict where elements of a are accessed through the value of key
  def arraytodict(input, key)
    input.map{|e| [e[key], e]}.to_h 
  end
  def dicttoarray(input)
    input.map{|k,e| e}
  end
  # merge two hashes into one
  def mergeHashes(input1, input2)
    res = {} 
    input1.each{|k,e| res[k]=e}
    input2.each{|k,e| 
      res[k] = {} unless res[k]
      res[k] = e.merge(res[k]) 
    }
    res
  end
  # input is an array of hash
  # output is the same array containing only keys as keys for all the elements 
  def selectSomeKeys(input, keys) 
    res = []
    input.each{|e| 
      h = {}
      keys.each{|k| h[k] = e[k] if e[k]}
      res += [h]
    }
    res
  end
  
  # inputs and keys are arrays, output is hash, each key match input
  def arraydict(input,keys)
    keys.map.with_index{ |k,i| [k, input[i%input.size]] }.to_h
  end
  
  def collect(input, key) 
    input.map{|el| el[key]}.uniq.compact # compact remove nil values
  end
 
  def svg2(input,perCent,keys,cl,dx=20,dy=0,prop=0.8)
    res = ""
    keys.each{|k|
      zone = ""
      w = 0
      w = perCent[k]*prop if perCent[k]
      zone = %{
<rect class="#{cl[k]}" x="#{dx}%" y="#{dy}" width="#{w}%" height="20" />
<text text-anchor="middle" class="#{cl[k]}" x="#{dx+w/2}%" y="#{dy+15}">#{input[k]}</text>
      } if w>0 
      dx += w 
      res += zone
    }
    res
  end

  def svg(input,keys,cl,txt,dx=20,dy=0,prop=80)
    perCent = proportion(input)
    data = concatstr(cat(cat(input, txt, ' '), roundh(perCent,0), ' ('), '%)') 
    svg2(data, perCent, keys, cl, dx, dy, prop/100.0)
  end

end

Liquid::Template.register_filter(Jekyll::Sum)
