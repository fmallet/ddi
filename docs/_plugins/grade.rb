# Declare module of your plugin under Jekyll module
module Jekyll::Grade
  def dept(input)
    input = input.gsub('DPT ', '') if input
    input = input.gsub('DEPARTEMENT ', '') if input
  end
  def grade(input)
    input = input.gsub('(Aucun ou inconnu)', '-')
    input = input.gsub('MAITRE DE CONFERENCE', 'MC')
    input = input.gsub('PROFESSEUR DES UNIVERSITES', 'PU')
    input = input.gsub('PROFESSEUR AGREGE', 'PRAG')
    input = input.gsub('PROFESSEUR CERTIFIE', 'PRCE')
    input = input.gsub('NORMALE', 'N')
    input = input.gsub('CLASSE', 'CL.')
    input = input.gsub('EXCEPTIONNELLE', 'Ex')
  end
end

Liquid::Template.register_filter(Jekyll::Grade)
