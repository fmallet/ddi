# Declare module of your plugin under Jekyll module
module Jekyll::Stats
  FORMAT = /([^-]*) - (.*)/ 

  def pourcent(part,total,text=" ")
   if part == 0 
     "-"
   else
     "#{part.round(2)}#{text}(#{(part*100/total).round(1)}%)"
   end
  end

  def add(a1, a2)
    [a1, a2].transpose.map{|x| x.inject{ 
      |a,b| 
         if b then a+b 
         else a 
         end 
    }}
  end

  def dept_cpt(line)
    res = line.match(FORMAT) { |m| m.captures[0] }
    res = line if res.nil?
    res unless res.nil?
  end

  def line(key, value, total)
    "**#{key}** | #{pourcent(value[0],total[0])} | #{(value[0]/192.0).round(2)} | #{pourcent(value[1], total[1])} | #{pourcent(value[2], total[2])}"
  end
  def lignegenre(key,value)
   t = value[0] + value[1]
   "#{key} | #{pourcent(value[1], t)} | #{pourcent(value[0], t)} | #{t}"
  end

  def update(dic, age)
    if dic[age]
      dic[age] += 1
    else
      dic[age] = 1
    end
  end 

  def ligne_age(keys, input)
      t = 0
      res = ""
      keys.each{|k|
         t += input[k] if input[k] 
         res = "#{res} | #{input[k]}" if input[k]
         res = "#{res} | -" if !input[k]
      };
      [t, res] 
  end

  def age(input)
    f = input.select{|v| v['age']}.collect{|v| [v['corps'], v['composante'], ((v['age']/5).floor())*5]};

    total = {}
    cpte = {}
    corps = {}

    f.each{|v|
       corps[v[0]] = {}
       c = dept_cpt(v[1])
       cpte[c] = {} if c
       cpte[c][v[2]] = 0 if c
    }
 
    f.each{|v| 
      update(corps[v[0]], v[2])
      update(total, v[2])
      c = dept_cpt(v[1])
      update(cpte[c], v[2]) if c
    }

    keys = total.keys.sort
    head = ""
    sep = ""
    keys.each{|k| 
      head = "#{head} | [#{k}-#{k+5}["
      sep = "#{sep} | ---"
    }; 
    cp = ""
    cpte.keys.sort.each{|k| 
      l = ligne_age(keys, cpte[k])
      cp = "#{cp}#{k} | #{l[0]} #{l[1]}\n"; 
    };
    co = ""
    corps.keys.sort.each{|k| 
      l = ligne_age(keys, corps[k])
      co = "#{co}#{k} | #{l[0]} #{l[1]}\n" if l[0]>1; 
    };

    r=%{

## Par composante

Cpt | Total #{head}
--- | --- #{sep}
#{cp}

## Par corps

Corps | Total #{head}
--- | --- #{sep}
#{co}
    }
  end

  # Each method of the module creates a custom Jekyll filter
  def hours(input)
    # Return result URL string
    f = input.select{|v| v['hour'] or v['hc']}.collect{|v| [v['corps'], v['composante'], v['hour'], v['decharge'], v['hc']]};
    corps = {}
    dept = {}
    cpte = {}
    total = [0]*3
    f.each{|v| 
      corps[v[0]]=[0]*3 
      dept[v[1]]=[0]*3 
      c = dept_cpt(v[1])
      cpte[c] = [0]*3 if c
    }
    f.each{|v| 
       corps[v[0]] = add(corps[v[0]], v[2..4])
       dept[v[1]]  = add(dept[v[1]],  v[2..4])
       c = dept_cpt(v[1])
       cpte[c] = add(cpte[c], v[2..4]) if c
       total = add(total, v[2..4])
    }
   
    c = corps.map{|key,value| line(key,value,total) }.sort
    d = dept.map{|key,value| line(key,value,total) }.sort
    cp = cpte.select{|key,value| value[0]>0}.map{|key,value| line(key,value,total) }.sort
 
    r=%{
# Potentiel d'enseignement

## Ensemble du département disciplinaire d'informatique

*(ETP=h/192)*

| Total | h | ETP |
| --- | --- | --- |
| **Service statutaire**  | #{total[0]} | #{(total[0]/192.0).round(2)} |
| **Modifications**  | #{total[1]} | #{(total[1]/192.0).round(2)} |
| **Effectif**   | #{total[0] - total[1]} |  #{((total[0] - total[1])/192.0).round(2)} |
| **HC 2019-2020** | #{total[2].round(2)} | #{(total[2]/192.0).round(2)} |

**Remarques:**
- Les modifications incluent les disponibilités, détachements, et décharges pour responsabilités administratives lourdes, quand elles ne sont pas converties en primes
- Le potentiel effectif est le service statutaire moins les modifications

## Par composante

Composante | Service (h) | Service (ETP) | Décharges (h) | HC (h)
 --- | --- | --- | --- | --- 
#{cp.join("\n")}

## Par corps

Corps | Service (h) | Service (ETP) | Décharges (h) | HC (h)
 --- | --- | --- | --- | --- 
#{c.join("\n")}

## Par département

Département | Service (h) | Service (ETP) | Décharges (h) | HC (h)
 --- | --- | --- | --- | --- 
#{d.join("\n")}
    }
  end
end

Liquid::Template.register_filter(Jekyll::Stats)
