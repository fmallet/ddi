---
layout: membres
composante: INRIA
---

**Date:** {{ site.data.ddi.date.day }}/{{ site.data.ddi.date.month }}/{{ site.data.ddi.date.year }}

{% for item in site.data.ddi.membres %}
  {% if item.composante contains page.composante %} 
- {{ item.name }} {{ item.firstname | capitalize }}
  {% endif %}
{% endfor %}
