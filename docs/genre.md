---
layout : stats
title : Répartition des genres 
---
{% assign k = "Harpege, Gender, corps, composante, departement" | split: ", " %}
{% assign h = site.data._2021.civil.harpege | selectSomeKeys: k %} 
{% assign g19 = site.data._2019.civil.harpege | selectSomeKeys: k | count: 'Gender' %} 
{% assign g20 = site.data._2020.civil.harpege | selectSomeKeys: k | count: 'Gender' %} 
{% assign gender = h | count: 'Gender' %} 
{% assign ke = "Mme, M." | split: ", " %}
{% assign cl = "female, male" | split: ", " | arraydict: ke %}
{% assign tx = "F, H" | split: ", " | arraydict: ke %}

## Ensemble du département disciplinaire informatique 

<svg width="100%" height="20">
  <text class="neutral" x="0" y="15" font-weight="bold">2019:</text> 
  <text class="neutral" x="40" y="15">{{g19["Mme"] | plus: g19["M."]}}</text> 
{{ g19 | svg: ke, cl, tx}}
</svg>
<svg width="100%" height="20">
  <text class="neutral" x="0" y="15" font-weight="bold">2020:</text> 
  <text class="neutral" x="40" y="15">{{g20["Mme"] | plus: g20["M."]}}</text> 
{{ g20 | svg: ke, cl, tx}}
</svg>
<svg width="100%" height="20">
  <text class="neutral" x="0" y="15" font-weight="bold">2021:</text> 
  <text class="neutral" x="40" y="15">{{gender["Mme"] | plus: gender["M."]}}</text> 
{{ gender | svg: ke, cl, tx}}
</svg>


{% assign hd = h | arraytodict: 'Harpege' %} 
{% assign data = site.data.ddi.membres | selectSomeKeys: k | arraytodict: 'Harpege' | mergeHashes: hd | dicttoarray %} 
{% assign cpte = site.data.ddi.membres | collect: 'composante' | sort %}
{% assign corps = site.data.ddi.membres | collect: 'corps' | sort %}
{% assign clc = "female, male" | split: ", " | arraydict: cpte %}
{% assign cptedict = cpte | arraydict: cpte %}

## Par composante 

<!--
{% assign repc = data | count: 'composante' %}
{% assign pc = repc | proportion %}
<svg width="100%" height="60">
{{cptedict | svg2: pc, cpte, clc, 0, 0, 1}} 
{{repc | svg2: pc, cpte, clc, 0, 20, 1}} 
{{pc | roundh: 0 | concatstr: ' %' | svg2: pc, cpte, clc, 0, 40, 1}} 
</svg>
-->
{% for c in cpte %}
{% assign dt = data | where: "composante", c | count: 'Gender' %}
{% assign t = dt['Mme'] | plus: dt['M.'] %}
<svg width="100%" height="20">
  <text class="neutral" x="0" y="15">{{c}}: {{t}}</text>{{ dt  | svg: ke, cl, tx }}
</svg>
{% endfor %}

## Par corps

{% for co in corps %}
{% assign dt = data | where: "corps", co | count: 'Gender' %}
{% assign t = dt['Mme'] | plus: dt['M.'] %}
<svg width="100%" height="20">
  <text class="neutral" x="0" y="15">{{co}}: {{t}}</text>{{ dt  | svg: ke, cl, tx }}
</svg>
{% endfor %}

##  Par département
{% for c in cpte %}
{% assign lm = data | where: "composante", c %}
{% assign dpt = lm | collect: 'departement' | sort %}
{% for d in dpt %}
{% assign dt = lm | where: "departement", d  | count: 'Gender' %}
{% assign t = dt['Mme'] | plus: dt['M.'] %}
<svg width="100%" height="40">
  <text text-anchor="start" class="neutral" x="0%" y="15" font-weight="bold">{{c}}</text>
  <text text-anchor="middle" class="neutral" x="55%" y="15">{{d | dept}}</text>
  <text text-anchor="middle" class="female" x="95%" y="15">{{t}}</text>
{{ dt  | svg: ke, cl, tx, 0, 20, 100 }}
</svg>
{% endfor %}
{% endfor %}
