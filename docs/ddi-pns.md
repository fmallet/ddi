---
layout: membres
composante: POLYTECH
dept: 
  - DPT GENIE BIOLOGIQUE (GB) 
  - DPT INGENIERIE SYSTEMES ELECTRONIQUES (ISE)
  - DPT SCIENCES INFORMATIQUES (SI)
---

**Date:** {{ site.data.ddi.date.day }}/{{ site.data.ddi.date.month }}/{{ site.data.ddi.date.year }}

*People on leave do not appear here.*

{% for d in page.dept %}

## {{ d }}

 {% for item in site.data.ddi.membres %}
  {% if item.composante contains page.composante %} 
  {% if item.departement contains d %}
- {{ item.name }} {{ item.firstname | capitalize }}
  {% endif %}
  {% endif %}
 {% endfor %}
{% endfor %}
