---
layout: membres
composante: EUR DS4H
dept: 
  - DEPARTEMENT INFORMATIQUE
---

**Date:** {{ site.data.ddi.date.day }}/{{ site.data.ddi.date.month }}/{{ site.data.ddi.date.year }}

*People on leave do not appear here.*

{% assign liste_c = site.data.ddi.membres | where: 'composante', page.composante %}
{% for d in page.dept %}
{% assign liste_d = liste_c | where: 'departement', d %}
## {{ d }}

 {% for item in liste_d %}
- {{ item.name }} {{ item.firstname | capitalize }}
 {% endfor %}
{% endfor %}
