---
layout : stats-c
title : Test civil 2020 
---

{% assign data2019 = site.data._2019.civil.harpege | arraytodict: "Harpege" %}
{% assign data2020 = site.data._2020.civil.harpege | arraytodict: "Harpege" %}
{% assign data = site.data._2020.civil %}
# civil.yml
- date: 
  - 2019 : {{ site.data._2019.civil.metadata.date }} 
  - 2020 : {{ data.metadata.date }} 
- size: 
  - 2019 : {{ data2019 | size }}
  - 2020 : {{ data.harpege | size }}

{% assign membres = data.harpege | sort: "name" %}

## Départs
{% for k in data2019 %}
  {% assign h = k[0] %}
  {% unless data2020[h] %}
- **{{ k[1].Harpege }}** : {{ k[1].gender }} {{ k[1].Name }}
  {% endunless %}
{% endfor %}
## Arrivées
{% for m in membres %}
  {% unless data2019[m.Harpege] %}
- **{{ m.Harpege }}** : {{ m.gender }} {{ m.Name }}
  {% endunless %}
{% endfor %}
## Membres 2020-2021
{% for m in membres %}
  {% if m.Harpege %}
- **{{ m.Harpege }}** : {{ m.gender }} {{ m.Name }}
  {% endif %}
{% endfor %}
