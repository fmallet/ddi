---
layout : stats-c
title : Test ddi 2020
---
{% assign data2019 = site.data._2019.ddi.membres | arraytodict: "Harpege" %}
{% assign data2020 = site.data._2020.ddi.membres | arraytodict: "Harpege" %}
{% assign data = site.data._2020.ddi %}
# ddi.yml 2020
- date: {{ data.date.day }} / {{ data.date.month }} / {{data.date.year}}
- size: 
  - 2019: {{ data2019 | size }}
  - 2020: {{ data.membres | size }}

## Départs
{% for k in data2019 %}
  {% assign h = k[0] %}
  {% unless data2020[h] %}
**{{ k[1].Harpege }}** : {{ k[1].gender }} {{ k[1].firstname }} {{ k[1].name }}
  {% endunless %}
{% endfor %}

{% assign membres = data.membres | sort: "name" %}
## Sans Numéro Harpege
{% for m in membres %}
  {% unless m.Harpege %}
- {{ m.gender }} {{ m.firstname }} {{ m.name }}
  {% endunless %}
{% endfor %}

## Membres
{% for m in membres %}
  {% if m.Harpege %}
- **{{ m.Harpege }}** : {{ m.gender }} {{ m.firstname }} {{ m.name }}
  {% endif %}
{% endfor %}
