---
layout : stats-c
title : Test ddi 2021
---
{% assign databefore = site.data._2020.ddi.membres | arraytodict: "Harpege" %}
{% assign datanow = site.data._2021.ddi.membres | arraytodict: "Harpege" %}
{% assign data = site.data._2021.ddi %}
# ddi.yml 2021
- date: {{ data.date.day }} / {{ data.date.month }} / {{data.date.year}}
- size: 
  - 2020: {{ databefore | size }}
  - 2021: {{ data.membres | size }}

## Départs
{% for k in databefore %}
  {% assign h = k[0] %}
  {% unless datanow[h] %}
**{{ k[1].Harpege }}** : {{ k[1].gender }} {{ k[1].firstname }} {{ k[1].name }}
  {% endunless %}
{% endfor %}

## Arrivées
{% for k in datanow %}
  {% assign h = k[0] %}
  {% unless databefore[h] %}
**{{ k[1].Harpege }}** : {{ k[1].gender }} {{ k[1].firstname }} {{ k[1].name }}
  {% endunless %}
{% endfor %}

{% assign membres = data.membres | sort: "name" %}
## Sans Numéro Harpege
{% for m in membres %}
  {% unless m.Harpege %}
- {{ m.gender }} {{ m.firstname }} {{ m.name }}
  {% endunless %}
{% endfor %}

## Membres
{% for m in membres %}
  {% if m.Harpege %}
- **{{ m.Harpege }}** : {{ m.gender }} {{ m.firstname }} {{ m.name }}
  {% endif %}
{% endfor %}
