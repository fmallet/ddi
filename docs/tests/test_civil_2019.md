---
layout : stats-c
title : Test civil 2019 
---
{% assign data = site.data._2019.civil %}
# civil.yml 2019
- date: {{ data.metadata.date }} 
- size: {{ data.harpege | size }}

{% assign membres = data.harpege | sort: "name" %}

## Membres 2019-2020
{% for m in membres %}
  {% if m.Harpege %}
- **{{ m.Harpege }}** : {{ m.gender }} {{ m.Name }}
  {% endif %}
{% endfor %}
