---
layout : stats-c
title : Test ddi 2019
---
{% assign data = site.data._2019.ddi %}
# ddi.yml 2019
- date: {{ data.date[0].day }} / {{ data.date[1].month }} / {{data.date[2].year}}
- size: {{ data.membres | size }}

{% assign membres = data.membres | sort: "name" %}
## Sans Numéro Harpege
{% for m in membres %}
  {% unless m.Harpege %}
- {{ m.gender }} {{ m.firstname }} {{ m.name }}
  {% endunless %}
{% endfor %}

## Membres
{% for m in membres %}
  {% if m.Harpege %}
- **{{ m.Harpege }}** : {{ m.gender }} {{ m.firstname }} {{ m.name }}
  {% endif %}
{% endfor %}
