#!/bin/bash
echo -e "baseurl: ./\n$(cat _config.yml)" > docs/_config.yml
cd docs
bundle exec jekyll build
rsync -rz _site/ fmallet@bastion.i3s.unice.fr:/net0/www/fmallet/public_html/ddi
cd ..
