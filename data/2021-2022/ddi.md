**Date:** {{ site.data.ddi.date.day }}/{{ site.data.ddi.date.month }}/{{ site.data.ddi.date.year }}
##  IUT

|  INFORMATIQUE |  |  | 13 |
|---|---|---|---|
|ACUNDEGER Erol|BLAY Mireille|BRENAC Lise|CAILLOUET Christelle|
|DONATI Leo|FERRY Nicolas|MOULIERAC Joanna|PALLEZ Denis|
|PALLEZ-DARTIGUES Christel|PERALDI-FRATI Marie agnes|POURCELOT Nicolas|REY Gaetan|
|SYSKA Michel|||

|  QUALITE LOGISTIQUE INDUSTRIELLE ET ORGANISATION |  |  | 3 |
|---|---|---|---|
|BENSMAIL Julien|FEDELE Carine|HUET Fabrice|

|  RESEAUX ET TELECOMMUNICATION |  |  | 5 |
|---|---|---|---|
|BOUDAOUD Karima|ESCAZUT Cathy|GAUTERO Michel|SASSATELLI Lucile|
|URVOY-KELLER Guillaume|||

|  STATISTIQUE ET INFORMATIQUE DECISIONNELLE |  |  | 1 |
|---|---|---|---|
|DA COSTA PEREIRA Celia|||

|  GESTION DES ENTREPRISES ET DES ADMINISTRATIONS |  |  | 1 |
|---|---|---|---|
|GARCIA Christine|||

|  TECHNIQUE DE COMMERCIALISATION NICE |  |  | 1 |
|---|---|---|---|
|HOHWILLER Luc|||

**Total:** 24 **H :** 12 (50.00%) **F :**  12 (50.00%)
##  INRIA

|  |  |  | 3 |
|---|---|---|---|
|ALLIEZ Pierre|ALOUF Sara|BERTOT Yves|

|  DIANA |  |  | 2 |
|---|---|---|---|
|BARAKAT Chadi|LEGOUT Arnaud||

|  STARS |  |  | 1 |
|---|---|---|---|
|BREMOND Francois|||

|  SEISM |  |  | 1 |
|---|---|---|---|
|CAZALS Frederic|||

|  COATI |  |  | 2 |
|---|---|---|---|
|COUDERT David|NISSE Nicolas||

|  ACENTAURI |  |  | 1 |
|---|---|---|---|
|MARTINET Philippe|||

|  BioVision |  |  | 1 |
|---|---|---|---|
|WU Hui-yin|||

**Total:** 11 **H :** 9 (81.82%) **F :**  2 (18.18%)
##  EUR DS4H

|  INFORMATIQUE |  |  | 34 |
|---|---|---|---|
|APARICIO PARDO Ramon|BALDELLON Olivier|BEAUQUIER Bruno|BUFFA Michel|
|CABRIO Elena|~~CAROMEL Denis~~|CRESCENZO Pierre|~~DALLE Olivier~~|
|DE MARIA Elisabetta|DI GIUSTO Cinzia|FORMENTI Enrico|GUINGNE Franck|
|JULIA Sandrine|KOUNALIS Emmanuel|LAHIRE Philippe|LEZOWSKI Pierre|
|LOZES Etienne|~~LOZI Jean-pierre~~|MALAPERT Arnaud|MALLET Frederic|
|MARTIN Bruno|MENEZ Gilles|MIRBEL Isabelle|MOPOLO Gabriel|
|PASQUIER Nicolas|PELLEAU Marie|PROVILLARD Julien|REGIN Jean-charles|
|RENEVIER Philippe|SAUVAGE Nathalie|TETTAMANZI Andrea|TOUATI Sid|
|WINTER Michel|XU Chuan||

**Total:** 31 **H :** 26 (83.87%) **F :**  8 (25.81%) **On leave:** 3
##  POLYTECH

|  SCIENCES INFORMATIQUES (SI) |  |  | 31 |
|---|---|---|---|
|BAUDE Francoise|BOND Ioan|CAMINADA Alexandre|COLLAVIZZA Helene|
|COLLET Philippe|COUSTE Mathias|DEANTONI Julien|FARON Catherine|
|GAETANO Marc|GALLESIO Erick|~~HERMENIER Fabien~~|LAVIROTTE Stephane|
|LINGRAND Diane|LIPPI Sylvain|LITOVSKY Igor|LOPEZ Dino|
|MARTINET Jean|MOLINES Guilhem|~~MOSSER Sebastien~~|OCCELLO Audrey|
|PAPAZIAN Christophe|PINNA Anne marie|POURTIER Remi|PRECIOSO Frederic|
|RENARD Helene|RIVEILL Michel|ROUDIER Yves|SANDER Peter|
|TIGLI Jean yves|URSO Pascal|WINCKLER Marco|

|  GENIE BIOLOGIQUE (GB) |  |  | 2 |
|---|---|---|---|
|BERNOT Gilles|COMET Jean-paul||

|  INGENIERIE SYSTEMES ELECTRONIQUES (ISE) |  |  | 1 |
|---|---|---|---|
|GRANET Vincent|||

**Total:** 32 **H :** 27 (84.38%) **F :**  7 (21.88%) **On leave:** 2
##  CNRS

|  Section 6 |  |  | 4 |
|---|---|---|---|
|GIROIRE Frederic|HOGIE Luc|MONTAGNAT Johan|VILLATA Serena|


|  Section 7 |  |  | 1 |
|---|---|---|---|
|LEBRUN Jerome|||

**Total:** 5 **H :** 4 (80.00%) **F :**  1 (20.00%)
##  UFR LASH

|  INFORMATIQUE |  |  | 1 |
|---|---|---|---|
|REGOURD Jean-pierre|||

**Total:** 1 **H :** 1 (100.00%) **F :**  0 (0.00%)
