#!/usr/bin/python3

import sys

def clean(str):
   return str.strip("\s\"").replace(' ', '-').lower().capitalize()

def isZero(str):
  if str == "0":
    return True
  return False

def isMale(str):
  if str == "Monsieur":
     return True
  return False

if __name__ == "__main__":
  if len(sys.argv) < 2:
    print(f"Usage: {sys.argv[0]} [filename]")
    exit(1);

f = open(sys.argv[1], "r")
head = f.readline().strip()
body = f.readlines();

people = [ l.strip().split(";") for l in body ]
people = [ [p[1], p[2], p[8], p[3], p[10] ] for p in people ]
people.sort()
dict = {}
for n,p,c,g,q in people:
  dash = c.find('-')
  if dash == -1:
    cpte = c
    dept = ""
  else:
    cpte = c[:dash-1]
    dept = c[dash+1:]

  entry = [ n,p,isMale(g),isZero(q) ]
  if cpte in dict: 
    deptval = dict[cpte]
    if dept in deptval: deptval[dept] += [ entry ]
    else: deptval[dept] = [ entry ]
  else: 
    deptval = {}
    deptval[dept] = [ entry ] 
    dict[cpte] = deptval
 
#print(dict)

print("**Date:** {{ site.data.ddi.date.day }}/{{ site.data.ddi.date.month }}/{{ site.data.ddi.date.year }}")

for kc in dict:
  print()
  print('## ', kc) 
  print() # to avoid problems with tabular in md, needs an empty line before
  composante = dict[kc]
  male = female = total = inactive = 0
  for kd in composante:
    dept = composante[kd]
    kd = kd.replace("DPT ", "").replace("DEPARTEMENT ", "")
    c = 0
    print(f"| {kd} |  |  | {len(dept)} |")
    print('|---|---|---|---|')
    sep = '|'
    for n,p,is_male,is_inactive in dept:
      c=c+1
      full = n + ' ' + p.capitalize() 
      if is_inactive: 
        full = '~~' + full + '~~'
        inactive+=1
      if c%4==0: print(f"{sep}{full}|")
      else: 
         print(f"{sep}{full}", end='')
      if is_male: male+=1
      else: female+=1
      total+=1 
    while c%4!=0: 
      print(sep, end='')
      c = c+1
    print()
    print()
  print(f"**Total:** {total-inactive} " + \
        f"**H :** {male} ({male*100/(total-inactive):.2f}%) " + \
        f"**F :**  {female} ({female*100/(total-inactive):.2f}%)" +\
        (f" **On leave:** {inactive}" if inactive>0 else f""))
