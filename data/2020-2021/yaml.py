#!/usr/bin/python3

import sys

def clean(str):
   return str.strip("\s\"").replace(' ', '-').lower().capitalize()

def isMale(str):
  if str == "Monsieur":
     return True
  return False

def cpt_dpt(line):
  harpege = line[1]
  head = line[2:5]+line[6:9]
  cpt = line[9]
  cpt = line[9].split(" - ")
  if len(cpt)==1: cpt += [""] 
  tail = line[12:]
  return head + cpt + tail + [harpege]

if __name__ == "__main__":
  if len(sys.argv) < 2:
    print(f"Usage: {sys.argv[0]} [filename]")
    exit(1);

f = open(sys.argv[1], "r")
head = f.readline().strip()
body = f.readlines();

titles = [ 'name', 'firstname', 'gender', 'age',
          'corps', 'grade', 'composante', 'departement',
          'hour', 'decharge', 'section', 'hc', 'Harpege' ]
people = [ l.strip().split(";") for l in body ]
people = [ cpt_dpt(p) for p in people ]
people.sort()
pad = [None] * (len(people[0])-len(titles))
titles = titles + pad 

print('date:')

from time import gmtime, strftime
print('  day:', strftime('%d', gmtime()))
print('  month:', strftime('%m', gmtime()))
print('  year:', strftime('%Y', gmtime()))

print('membres:')

for p in people:
  print("  - ", end='')
  sep = ""
  for i,x in enumerate(p):
    if x != '':
      print(sep if titles[i] is None else sep+titles[i]+': ', x, sep='');
    sep = "    ";
