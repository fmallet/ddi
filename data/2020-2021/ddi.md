**Date:** {{ site.data.ddi.date.day }}/{{ site.data.ddi.date.month }}/{{ site.data.ddi.date.year }}
##  IUT

|  INFORMATIQUE |  |  | 15 |
|---|---|---|---|
|ACUNDEGER Erol|BLAY Mireille|BRENAC Lise|CAILLOUET Christelle|
|CHIGNOLI Robert|DONATI Leo|FERRY Nicolas|LECAT Rosa|
|MOULIERAC Joanna|PALLEZ Denis|PALLEZ-DARTIGUES Christel|PERALDI-FRATI Marie agnes|
|POURCELOT Nicolas|REY Gaetan|SYSKA Michel|

|  QUALITE LOGISTIQUE INDUSTRIELLE ET ORGANISATION |  |  | 3 |
|---|---|---|---|
|BENSMAIL Julien|FEDELE Carine|HUET Fabrice|

|  RESEAUX ET TELECOMMUNICATION |  |  | 5 |
|---|---|---|---|
|BOUDAOUD Karima|ESCAZUT Cathy|GAUTERO Michel|SASSATELLI Lucile|
|URVOY-KELLER Guillaume|||

|  STATISTIQUE ET INFORMATIQUE DECISIONNELLE |  |  | 1 |
|---|---|---|---|
|DA COSTA PEREIRA Celia|||

|  GESTION DES ENTREPRISES ET DES ADMINISTRATIONS |  |  | 1 |
|---|---|---|---|
|GARCIA Christine|||

|  TECHNIQUE DE COMMERCIALISATION NICE |  |  | 1 |
|---|---|---|---|
|HOHWILLER Luc|||

**Total:** 26 **H :** 13 (50.00%) **F :**  13 (50.00%)
##  INRIA

|  |  |  | 5 |
|---|---|---|---|
|ALLIEZ Pierre|CAZALS Frederic|COUDERT David|NISSE Nicolas|
|WU Hui-yin|||

**Total:** 5 **H :** 4 (80.00%) **F :**  1 (20.00%)
##  EUR DS4H

|  INFORMATIQUE |  |  | 35 |
|---|---|---|---|
|APARICIO PARDO Ramon|BALDELLON Olivier|BEAUQUIER Bruno|BUFFA Michel|
|CABRIO Elena|CAROMEL Denis|CRESCENZO Pierre|DALLE Olivier|
|DE MARIA Elisabetta|DI GIUSTO Cinzia|FORMENTI Enrico|GUINGNE Franck|
|JULIA Sandrine|KOUNALIS Emmanuel|LAHIRE Philippe|LEZOWSKI Pierre|
|LOZES Etienne|LOZI Jean-pierre|MALAPERT Arnaud|MALLET Frederic|
|MARTIN Bruno|MENEZ Gilles|MIRANDA Serge|MIRBEL Isabelle|
|MOPOLO Gabriel|PASQUIER Nicolas|PELLEAU Marie|PROVILLARD Julien|
|REGIN Jean-charles|RENEVIER Philippe|SABRI Oussama|SAUVAGE Nathalie|
|TETTAMANZI Andrea|TOUATI Sid|WINTER Michel|

**Total:** 35 **H :** 28 (80.00%) **F :**  7 (20.00%)
##  POLYTECH

|  SCIENCES INFORMATIQUES (SI) |  |  | 32 |
|---|---|---|---|
|BAUDE Francoise|BOND Ioan|CAMINADA Alexandre|COLLAVIZZA Helene|
|COLLET Philippe|COUSTE Mathias|DEANTONI Julien|FARON Catherine|
|GAETANO Marc|GALLESIO Erick|HERMENIER Fabien|LAVIROTTE Stephane|
|LINGRAND Diane|LIPPI Sylvain|LITOVSKY Igor|LOPEZ Dino|
|MARTINET Jean|MOLINES Guilhem|MOSSER Sebastien|OCCELLO Audrey|
|PAPAZIAN Christophe|PEYRAT Claudine|PINNA Anne marie|POURTIER Remi|
|PRECIOSO Frederic|RENARD Helene|RIVEILL Michel|ROUDIER Yves|
|SANDER Peter|TIGLI Jean yves|URSO Pascal|WINCKLER Marco|


|  GENIE BIOLOGIQUE (GB) |  |  | 2 |
|---|---|---|---|
|BERNOT Gilles|COMET Jean-paul||

|  INGENIERIE SYSTEMES ELECTRONIQUES (ISE) |  |  | 1 |
|---|---|---|---|
|GRANET Vincent|||

**Total:** 35 **H :** 27 (77.14%) **F :**  8 (22.86%)
##  UFR LASH

|  INFORMATIQUE |  |  | 2 |
|---|---|---|---|
|ENEE Gilles|REGOURD Jean-pierre||

**Total:** 2 **H :** 2 (100.00%) **F :**  0 (0.00%)
##  CNRS

|  Section 6 |  |  | 4 |
|---|---|---|---|
|GIROIRE Frederic|HOGIE Luc|MONTAGNAT Johan|VILLATA Serena|


|  Section 7 |  |  | 1 |
|---|---|---|---|
|LEBRUN Jerome|||

**Total:** 5 **H :** 4 (80.00%) **F :**  1 (20.00%)
