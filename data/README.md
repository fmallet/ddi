# Data folder

Some scripts are called from the [parent folder](..)

## Email list

- see [2019-2020](2019-2020/)
- [email.py](2019-2020/email.py): script to transform RH27.csv into RH27.txt
- **RH27.txt**: generated from RH27.csv
  - Source: `python3 email.py TH27.csv > RH27.csv` 

to be used to feed the SYMPA mailing-list website (Email first_name last_name)

- **RH27.csv**: Liste des membres du DDI 
  - Source: ? Obsolete ? 

---

## List of members

FicheRH27.csv is used to produce a full list of members categorized by components.

- **FicheRH27.csv**: extracted from Excel file
- **ddi.md**: generated from FicheRH27.csv
- **script.py**: transform FicheRH27.csv into ddi.md
  - `python3 script.py Fiche27.csv > ddi.md`
- **update_liste.sh**: bash script to call script.py and produce ddi.md
- **../update_members.sh**: calls update_list.sh, update_yaml.sh and cp ddi.md into the website structure

---

## YAML data

FicheRH27.csv is also used to produce statistics about the ddi. 

- ../docs/_data/ddi.yml: contains the same information in YAML format
- **update_yaml.sh**: call yaml.py to produce a ddi.yml in docs/_data/
- **yaml.py**: transform csv file into YAML format

## Statistiques

- s.yml and c.yml: docs/\_data/service.yml and docs/\_data/civil.yml are linked to these files and used on the site 
- civil.yml: produced by the Java program from OSE. Have to copy to c.yml to deploy
- service-2019-2-1.yml: produced by the Java program from OSE. Have to copy to s.yml.

