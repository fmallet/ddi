#!/usr/bin/python3

import sys

def clean(str):
   return str.strip("\s\"").replace(' ', '-').lower().capitalize()

if __name__ == "__main__":
  if len(sys.argv) < 2:
    print(f"Usage: {sys.argv[0]} [filename]")
    exit(1);

f = open(sys.argv[1], "r")
head = f.readline().strip()
body = f.readlines();

people = [ l.strip().split(";") for l in body ]
people.sort()
people = [ [clean(name), clean(forename)] for name,forename in people ]

mail = [ [f+'.'+n+'@univ-cotedazur.fr', f, n] for n,f in people ]
res = [ ' '.join(m) for m in mail ]
#print(head)
#print(len(body))
#print(body[0])
#print(people[0])
#print(mail[0])
#print(res[0])
print('\n'.join(res))


