#!/usr/bin/python3

import sys

def clean(str):
   return str.strip("\s\"").replace(' ', '-').lower().capitalize()

def isMale(str):
  if str == "Monsieur":
     return True
  return False

if __name__ == "__main__":
  if len(sys.argv) < 2:
    print(f"Usage: {sys.argv[0]} [filename]")
    exit(1);

f = open(sys.argv[1], "r")
head = f.readline().strip()
body = f.readlines();

people = [ l.strip().split(";") for l in body ]
people = [ [p[2], p[3], p[9], p[4] ] for p in people ]
people.sort()
dict = {}
for n,p,c,g in people:
  dash = c.find('-')
  if dash == -1:
    cpte = c
    dept = ""
  else:
    cpte = c[:dash-1]
    dept = c[dash+1:]

  if cpte in dict: 
    deptval = dict[cpte]
    if dept in deptval: deptval[dept] += [ [ n,p,isMale(g) ] ]
    else: deptval[dept] = [ [n,p,isMale(g)] ]
  else: 
    deptval = {}
    deptval[dept] = [[n,p,isMale(g)]] 
    dict[cpte] = deptval
 
#print(dict)

print("**Date:** {{ site.data.ddi.date[0].day }}/{{ site.data.ddi.date[1].month }}/{{ site.data.ddi.date[2].year }}")

for kc in dict:
  print('## ', kc) 
  print() # to avoid problems with tabular in md, needs an empty line before
  composante = dict[kc]
  male = female = total = 0
  for kd in composante:
    dept = composante[kd]
    kd = kd.replace("DPT ", "").replace("DEPARTEMENT ", "")
    c = 0
    print(f"| {kd} |  |  | {len(dept)} |")
    print('|---|---|---|---|')
    sep = '|'
    for n,p,is_male in dept:
      c=c+1
      full = n + ' ' + p.capitalize() 
      if c%4==0: print(f"{sep}{full}|")
      else: 
         print(f"{sep}{full}", end='')
      if is_male: male+=1
      else: female+=1
      total+=1 
    while c%4!=0: 
      print(sep, end='')
      c = c+1
    print()
    print()
  print(f"**Total:** {total} " + \
        f"**H :** {male} ({male*100/total:.2f}%) " + \
        f"**F :**  {female} ({female*100/total:.2f}%)")
